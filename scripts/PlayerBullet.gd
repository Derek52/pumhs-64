extends Area2D

var speed

export (float) var bulletLifetime

var velocity = Vector2()

func _ready():
	$Lifetime.wait_time = bulletLifetime

func start(_position, _direction, _bulletSpeed):
	position = _position
	speed = _bulletSpeed
	velocity = _direction * speed
	$Lifetime.start()

func _process(delta):
	position += velocity * delta
	
	if position.y < -1:
		queue_free()

func _on_PlayerBullet_body_entered(body):
	if body.has_method('getKilled'):
		body.getKilled()
		queue_free()

func _on_Lifetime_timeout():
	queue_free()
