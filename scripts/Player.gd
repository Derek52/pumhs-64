extends KinematicBody2D

signal shoot
signal gameOver

export (float) var MOVEMENT_SPEED = 10
export (PackedScene) var Bullet
export (float) var gunCooldown = 1
export (float) var bulletSpeed = 20

var velocity = Vector2()
var canShoot : bool = true
var playing : bool = false

func _ready():
	$GunTimer.wait_time = gunCooldown


func _process(delta):
	if playing:
		if Input.is_action_pressed("moveLeft"):
			velocity.x = -MOVEMENT_SPEED
		elif Input.is_action_pressed("moveRight"):
			velocity.x = MOVEMENT_SPEED
		else:
			velocity.x = 0
	
		if Input.is_action_pressed("moveUp"):
			velocity.y = -MOVEMENT_SPEED
		elif Input.is_action_pressed("moveDown"):
			velocity.y = MOVEMENT_SPEED
		else:
			velocity.y = 0
	
		if Input.is_action_pressed("shoot"):
			shoot()
	
		position += velocity * delta
		position.x = clamp(position.x, 0, 61)
		position.y = clamp(position.y, 0, 66)
	
func shoot():
	if canShoot:
		canShoot = false
		var dir = Vector2(0, -1)
		emit_signal('shoot', Bullet, $GunBarrel.global_position, dir, bulletSpeed)
		$GunTimer.start()

func _on_GunTimer_timeout():
	canShoot = true

func gameOver():
	$CollisionShape2D.disabled = true
	hide()
	emit_signal('gameOver')
	canShoot = false
	$GunTimer.stop()
	$DeadSound.play()

func reset():
	$CollisionShape2D.disabled = false
	position = Vector2(30.5, 56)
	canShoot = true
	show()
	

func _on_Area2D_body_entered(body):
	if body.has_method('getKilled'):
		gameOver()


#func _on_DeadSound_finished():
	#gameOver()
