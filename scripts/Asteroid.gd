extends StaticBody2D

signal destroyed

export (float) var speed
export (int) var leftScreenScore
export (int) var destroyedScore

var velocity = Vector2()
var scalar

func _ready():
	velocity.y = speed
	scalar = rand_range(2, 5)
	scale = Vector2(scalar, scalar)

func _process(delta):
	position += velocity * delta
	rotation += 5 * delta
	
	if position.y > 66:
		emit_signal('destroyed', floor(leftScreenScore / scalar))
		queue_free()
		
func getKilled():
	hide()
	emit_signal("destroyed", floor(destroyedScore / scalar))
	$DeadSound.play()
	

func _on_DeadSound_finished():
	queue_free()
