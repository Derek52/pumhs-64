extends Area2D

var speed

var velocity = Vector2()

export (float) var bulletLifetime

func _ready():
	$Lifetime.wait_time = bulletLifetime

func start(_position, _direction, _bulletSpeed):
	position = _position
	speed = _bulletSpeed
	velocity = _direction * speed
	$Lifetime.start()

func _process(delta):
	position += velocity * delta


func _on_Lifetime_timeout():
	queue_free()

func _on_Bullet_body_entered(body):
	if body.get_name() == "Player":
		body.gameOver()
