extends Node2D

signal updateScore

export (PackedScene) var Enemy
export (PackedScene) var Asteroid

export (float) var enemySpawnRate = 2

var score = 0

var playingGame = false

func _ready():
	randomize()

func _process(delta):
	if playingGame:
		if Input.is_action_just_pressed('resetGame'):
			reset()
		if delta > 0:
			pass#print(1/delta)
	else:
		if Input.is_action_just_pressed("startGame"):
			playingGame = true
			$HUD/StartLabel.hide()
			$EnemySpawnTimer.wait_time = enemySpawnRate
			$EnemySpawnTimer.start()
			$Player.playing = true
			$StartSound.play()

func _onShoot(bullet, position, direction, bulletSpeed):
	var b = bullet.instance()
	$Bullets.add_child(b)
	b.start(position, direction, bulletSpeed)

func _on_EnemySpawnTimer_timeout():
	$EnemyPath/EnemySpawnLocation.set_offset(randi())
	var diceRoll = rand_range(0, 1)
	if diceRoll > 0.7:
		var enemy = Enemy.instance()
		$Obstacles.add_child(enemy)
		enemy.connect("shoot", self, "_onShoot")
		enemy.connect('destroyed', self, 'onScore')
		enemy.position = $EnemyPath/EnemySpawnLocation.position
	else:
		var asteroid = Asteroid.instance()
		$Obstacles.add_child(asteroid)
		asteroid.connect('destroyed', self, 'onScore')
		asteroid.position = $EnemyPath/EnemySpawnLocation.position
	$EnemySpawnTimer.start()
	
func onScore(pointsScored):
	score += pointsScored
	emit_signal("updateScore", score)

func gameOver():
	$EnemySpawnTimer.stop()
	for child in $Obstacles.get_children():
		child.queue_free()
	for bullet in $Bullets.get_children():
		bullet.queue_free()
		
func reset():
	$Player.reset()
	emit_signal("updateScore", 0)
	$EnemySpawnTimer.start()
	$StartSound.play()