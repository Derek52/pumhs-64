extends StaticBody2D

signal shoot
signal destroyed

export (PackedScene) var Bullet
export (float) var bulletSpeed
export (float) var cooldownTime
export (float) var speed
export (int) var killScore
export (int) var leftScreenScore

var velocity = Vector2()
var canShoot : bool = true

func _ready():
	$cooldownTimer.wait_time = cooldownTime
	velocity.y = speed * 1

func _process(delta):
	position += velocity * delta
	
	if canShoot:
		canShoot = false
		shoot()
		
	if position.y > 70:
		emit_signal("destroyed", leftScreenScore)
		queue_free()

func shoot():
	var dir = Vector2(.5, 1)
	emit_signal('shoot', Bullet, $GunBarrel.global_position, dir, bulletSpeed)
	dir = Vector2(-.5, .5)
	emit_signal('shoot', Bullet, $GunBarrel.global_position, dir, bulletSpeed)
	$cooldownTimer.start()

func _on_cooldownTimer_timeout():
	canShoot = true

func getKilled():
	hide()
	emit_signal("destroyed", killScore)
	$DeadSound.play()

func _on_DeadSound_finished():
	queue_free()
